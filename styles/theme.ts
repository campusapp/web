import { createTheme } from "@mui/material";

export const primaryFont = "'Open-Sans', sans-serif";
export const secondaryFont = "'Raleway', sans-serif";

const pxToRem = (value: number) => {
  return `${value / 16}rem`;
};

const responsiveFontSizes = ({
  sm,
  md,
  lg,
}: {
  sm: number;
  md: number;
  lg: number;
}) => {
  return {
    "@media (min-width:600px)": {
      fontSize: pxToRem(sm),
    },
    "@media (min-width:960px)": {
      fontSize: pxToRem(md),
    },
    "@media (min-width:1280px)": {
      fontSize: pxToRem(lg),
    },
  };
};

export const theme = createTheme({
  palette: {
    common: {
      black: "#535353",
      white: "#fff",
    },
    primary: {
      light: "#78DCAD",
      main: "#35D78B",
      dark: "#0E9857",
      contrastText: "#fff",
    },
    secondary: {
      light: "#083E64",
      main: "#061621",
      dark: "#00080E",
      contrastText: "#fff",
    },
  },
  components: {
    MuiAppBar: {
      styleOverrides: {
        colorDefault: {
          background: "white",
          boxShadow: "unset",
          height: "4rem",
        },
      },
    },
    MuiPaper: {
      styleOverrides: {
        root: {
          padding: "2rem",
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: "999rem",
          paddingRight: "1.5rem",
          paddingLeft: "1.5rem",
          boxShadow: "none",
          margin: "0",
          width: "max-content",
          "&:hover": {
            boxShadow: "none",
          },
          "&:active": {
            boxShadow: "none",
          },
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        root: {
          fontFamily: primaryFont,
          fontWeightRegular: 400,
          fontWeightMedium: 600,
          fontWeightBold: 700,
        },
        h1: {
          fontFamily: secondaryFont,
          textTransform: "uppercase",
          fontWeight: 700,
          lineHeight: 80 / 64,
          fontSize: pxToRem(40),
          ...responsiveFontSizes({ sm: 52, md: 58, lg: 64 }),
        },
        h2: {
          fontFamily: secondaryFont,
          textTransform: "uppercase",
          fontWeight: 700,
          lineHeight: 64 / 48,
          fontSize: pxToRem(40),
          ...responsiveFontSizes({ sm: 40, md: 44, lg: 48 }),
        },
        h3: {
          fontFamily: secondaryFont,
          fontWeight: 700,
          lineHeight: 1.5,
          fontSize: pxToRem(24),
          ...responsiveFontSizes({ sm: 26, md: 30, lg: 32 }),
        },
        h4: {
          fontFamily: secondaryFont,
          fontWeight: 700,
          lineHeight: 1.5,
          fontSize: pxToRem(20),
          ...responsiveFontSizes({ sm: 20, md: 24, lg: 24 }),
        },
        h5: {
          fontFamily: secondaryFont,
          fontWeight: 700,
          lineHeight: 1.5,
          fontSize: pxToRem(18),
          fontStyle: "italic",
          ...responsiveFontSizes({ sm: 19, md: 20, lg: 20 }),
        },
        h6: {
          fontFamily: secondaryFont,
          fontWeight: 500,
          lineHeight: 28 / 18,
          fontSize: pxToRem(17),
          fontStyle: "italic",
          ...responsiveFontSizes({ sm: 18, md: 18, lg: 18 }),
        },
        subtitle1: {
          fontWeight: 600,
          lineHeight: 1.5,
          fontSize: pxToRem(16),
        },
        subtitle2: {
          fontWeight: 600,
          lineHeight: 22 / 14,
          fontSize: pxToRem(14),
        },
        body1: {
          lineHeight: 1.5,
          fontSize: pxToRem(16),
        },
        body2: {
          lineHeight: 22 / 14,
          fontSize: pxToRem(14),
        },
        caption: {
          lineHeight: 1.5,
          fontSize: pxToRem(12),
        },
        overline: {
          fontWeight: 700,
          lineHeight: 1.5,
          fontSize: pxToRem(12),
          letterSpacing: 1.1,
          textTransform: "uppercase",
        },
        button: {
          fontFamily: secondaryFont,
          fontWeight: 700,
          lineHeight: 24 / 14,
          fontSize: pxToRem(14),
          textTransform: "uppercase",
        },
      },
    },
  },
});
