import {
  createContext,
  FC,
  useContext,
  useEffect,
  useMemo,
  useState,
} from "react";
import axios from "axios";

export const UserContext = createContext(undefined);

export const useUserContext = () => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error("useUserContext must be inside an UserProvider");
  }
  return context;
};

export const UserProvider: FC = ({ children }) => {
  const [userID, setUserID] = useState(null);

  useEffect(() => {
    const fetchToken = async () => {
      await axios({
        method: "get",
        //   url: `${process.env.REACT_APP_API_URL}/api/jwtid`,
        url: `http://localhost:4000/api/jwtid`,
        withCredentials: true,
      })
        .then((res) => {
          setUserID(res.data);
        })
        .catch((error) => console.error("No token"));
    };
    fetchToken();
  });

  const [user, setUser] = useState(null);

  useEffect(() => {
    const fetchUser = async () => {
      await axios({
        method: "get",
        //   url: `${process.env.REACT_APP_API_URL}/api/user/${userID}`,
        url: `http://localhost:4000/api/user/${userID}`,
        withCredentials: true,
      })
        .then((res) => {
          setUser(res.data);
        })
        .catch((error) => console.error("No user"));
    };
    fetchUser();
  }, [userID]);

  const value = useMemo(
    () => ({ userID, setUserID, user }),
    [userID, setUserID, user]
  );

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
};
