import { FC, useEffect, useState } from "react";
import {
  Button,
  Stack,
  TextField,
  Typography,
  Box,
  Stepper,
  Step,
  StepLabel,
  IconButton,
} from "@mui/material";
import { KeyboardArrowLeft, KeyboardArrowRight } from "@mui/icons-material";

import { useRouter } from "next/dist/client/router";
import { useRegisterClubContext } from "./_internal";

export const RegisterClubForm: FC = () => {
  const router = useRouter();
  const { steps, activeIndex, handleNext, handleBack, formik } =
    useRegisterClubContext();

  const [activeStep, setActiveStep] = useState(steps[activeIndex]);

  useEffect(() => {
    setActiveStep(steps[activeIndex]);
  }, [activeIndex]);

  return (
    <>
      <Typography
        variant="h4"
        component="h4"
        sx={{ textAlign: "center", maxWidth: "80%", lineHeight: 1, mb: 3 }}
      >
        Créer son compte club/entreprise
      </Typography>
      <Box component="form" onSubmit={formik.handleSubmit}>
        <Box>
          <Stepper color="secondary" activeStep={activeIndex}>
            {steps.map((label, i) => {
              return (
                <Step color="secondary" key={i}>
                  <StepLabel color="secondary"></StepLabel>
                </Step>
              );
            })}
          </Stepper>
          <activeStep.Component />
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            mt: 3,
          }}
        >
          <IconButton
            color="inherit"
            disabled={activeIndex === 0}
            onClick={handleBack}
            sx={{ mr: 1 }}
          >
            <KeyboardArrowLeft />
          </IconButton>
          <Box sx={{ flex: "1 1 auto" }} />

          {activeIndex === steps.length - 1 ? (
            <Button
              variant="contained"
              color="secondary"
              type="submit"
              // loading={formik.isSubmitting}
            >
              Ajouter
            </Button>
          ) : (
            <Button size="small" variant="outlined" onClick={handleNext}>
              Suivant
              <KeyboardArrowRight />
            </Button>
          )}
        </Box>
      </Box>
    </>
  );
};
