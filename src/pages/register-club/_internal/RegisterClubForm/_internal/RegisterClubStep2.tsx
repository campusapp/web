import { FC, useState } from "react";

import { Button, Stack, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useRegisterClubContext } from "./RegisterClubContext";

export const RegisterClubStep2: FC = () => {
  return (
    <Box
      sx={{
        mt: 4,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Typography
        variant="body1"
        component="p"
        sx={{ textAlign: "center", maxWidth: "80%" }}
      >
        Step 2
      </Typography>
    </Box>
  );
};
