import axios from "axios";
import { useFormik } from "formik";
import {
  createContext,
  ElementType,
  useContext,
  useMemo,
  useState,
} from "react";

import { RegisterClubStep1 } from "./RegisterClubStep1";
import { RegisterClubStep2 } from "./RegisterClubStep2";

export const RegisterClubContext = createContext<
  RegisterClubContextProps | undefined
>(undefined);

export const useRegisterClubContext = (): RegisterClubContextProps => {
  const context = useContext(RegisterClubContext);
  if (!context) {
    throw new Error(
      "useRegisterClubContext must be inside a RegisterClubProvider"
    );
  }
  return context;
};

export type RegisterClubContextProps = {
  steps: [
    {
      Component: ElementType;
      hideNext?: boolean;
    }
  ];
  activeIndex: number;
  setActiveIndex: (activeIndex: number) => void;
  handleNext: () => void;
  handleBack: () => void;
  formik: ReturnType<typeof useFormik>;
  status: string;
};

export const RegisterClubProvider = ({ children }) => {
  const steps = [
    {
      Component: RegisterClubStep1,
      hideNext: true,
    },
    {
      Component: RegisterClubStep2,
    },
  ];

  const [activeIndex, setActiveIndex] = useState(0);

  const handleNext = () => {
    setActiveIndex((prevActiveIndex) => prevActiveIndex + 1);
  };

  const handleBack = () => {
    setActiveIndex((prevActiveIndex) => prevActiveIndex - 1);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      organization_type: "club",
      sport_id: "",
      address: "",
      logo: "",
    },
    onSubmit: async (values) => {
      try {
        await axios({
          method: "post",
          //   url: `${process.env.REACT_APP_API_URL}/api/user/login`,
          url: `http://localhost:4000/api/user/register`,
          withCredentials: true,
          data: {
            name: values.name,
            organization_type: values.organization_type,
            sport_id: values.sport_id,
            address: values.address,
            logo: values.logo,
          },
        });
        enqueueSnackbar("Tout se passe comme prévu", {
          variant: "success",
        });
      } catch (error) {
        console.error("error: ", error);
      }
    },
  });

  const value = useMemo(
    () => ({
      steps,
      activeIndex,
      setActiveIndex,
      handleNext,
      handleBack,
      formik,
    }),
    [activeIndex, setActiveIndex, handleNext, handleBack, formik]
  );

  return (
    <RegisterClubContext.Provider value={value}>
      {children}
    </RegisterClubContext.Provider>
  );
};
