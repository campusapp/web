import { FC, useState } from "react";

import { Button, Stack, TextField, Typography } from "@mui/material";
import { Box } from "@mui/system";
import { useRegisterClubContext } from "./RegisterClubContext";

export const RegisterClubStep1: FC = () => {
  const { formik } = useRegisterClubContext();

  return (
    <Box
      sx={{
        mt: 4,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Stack gap={2}>
        <TextField
          fullWidth
          label="Nom du club"
          variant="outlined"
          defaultValue={formik.values.name}
          onChange={(value) => formik.setFieldValue("name", value)}
        />
        <TextField
          fullWidth
          label="Adresse"
          variant="outlined"
          defaultValue={formik.values.address}
          onChange={(value) => formik.setFieldValue("address", value)}
        />
        <TextField
          fullWidth
          label="Sport"
          variant="outlined"
          defaultValue={formik.values.sport_id}
          onChange={(value) => formik.setFieldValue("sport_id", value)}
        />
        <TextField
          name="club-logo"
          // onChange={formik.handleChange}
          // value={formik.values.image}
          label="Logo du club"
          variant="outlined"
          fullWidth
          type="file"
        />
      </Stack>
    </Box>
  );
};
