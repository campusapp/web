import { Paper, Box } from "@mui/material";
import type { NextPage } from "next";
import { RegisterClubForm } from "./_internal/RegisterClubForm";
import { RegisterClubProvider } from "./_internal/RegisterClubForm/_internal";

const RegisterClub: NextPage = () => {
  return (
    <Box
      sx={{
        background: "#061621",
        height: "100vh",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "column",
      }}
    >
      <Box
        sx={{
          width: "auto",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
        }}
      >
        <Paper sx={{ minWidth: "40rem" }} elevation={0}>
          <RegisterClubProvider>
            <RegisterClubForm />
          </RegisterClubProvider>
        </Paper>
      </Box>
    </Box>
  );
};

export default RegisterClub;
