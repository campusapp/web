import { FC, useState } from "react";
import {
  Typography,
  Tabs,
  Tab,
  Box,
  Paper,
  Divider,
  Stack,
  Button,
} from "@mui/material";
import Head from "next/head";
import { useRouter } from "next/dist/client/router";
import { Badge } from "../../../../components";
import { Launch } from "@mui/icons-material";

export const InformationsTab: FC = () => {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>test</title>
      </Head>

      <Typography variant="body1">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ut
        eros et eros mattis sollicitudin. Curabitur sit amet posuere leo, in
        dignissim lectus. Aenean eget lacus nunc. Proin luctus urna vel mi
        mollis sagittis. Interdum et malesuada fames nisi.
      </Typography>
      <Divider sx={{ mt: 2, mb: 2 }} />
      <Typography variant="h4" sx={{ lineHeight: 1 }}>
        Badges & évaluations
      </Typography>
      <Stack
        sx={{ mt: 2 }}
        direction="row"
        alignItems="center"
        justifyContent="center"
        spacing={3}
      >
        <Badge />
        <Badge />
        <Badge />
        <Badge />
      </Stack>
      <Divider sx={{ mt: 2, mb: 2 }} />
      <Stack
        sx={{ mt: 2 }}
        alignItems="center"
        justifyContent="center"
        spacing={1}
      >
        <Button
          variant="contained"
          href="#"
          target="_BLANK"
          sx={{ alignSelf: "center", borderRadius: "unset", width: "100%" }}
          endIcon={<Launch fontSize="small" />}
        >
          Commander en ligne
        </Button>
        <Button
          variant="contained"
          href="#"
          target="_BLANK"
          sx={{ alignSelf: "center", borderRadius: "unset", width: "100%" }}
          endIcon={<Launch fontSize="small" />}
        >
          Voir le site internet
        </Button>
      </Stack>
    </>
  );
};
