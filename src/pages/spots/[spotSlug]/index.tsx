import {
  FavoriteBorder,
  Favorite,
  Launch,
  LocalOfferOutlined,
  KeyboardArrowRightOutlined,
  PlaceOutlined,
  LanguageOutlined,
} from "@mui/icons-material";
import {
  Typography,
  Box,
  Chip,
  Stack,
  IconButton,
  Divider,
  Button,
  TabPanelUnstyled,
  TabUnstyled,
  TabsUnstyled,
  TabsListUnstyled,
  tabUnstyledClasses,
  buttonUnstyledClasses,
  Link,
} from "@mui/material";
import type { NextPage } from "next";
import { useRouter } from "next/dist/client/router";
import { styled } from "@mui/system";
import { useState } from "react";
import { Badge, Header } from "../../../components";
import { InformationsTab } from "./_internal/InformationsTab";

const Tab = styled(TabUnstyled)`
  cursor: pointer;
  background-color: transparent;
  font-family: "Open-Sans", sans-serif;
  border: unset;
  margin-right: 1.5rem;
  font-size: 1rem;
  font-weight: 600;
  padding: 0;
  padding-bottom: 0.2rem;
  border-bottom: 3px #f2f2f2 solid;
  margin-top: 0.4rem;
  &:hover {
    background-color: unset;
    border-bottom: 0.2rem #21806d solid;
  }
  &.${buttonUnstyledClasses.focusVisible} {
    color: unset;
    outline: none;
    background-color: unset;
  }
  &.${tabUnstyledClasses.selected} {
    color: unset;
    border-bottom: 3px #21806d solid;
  }
  &.${buttonUnstyledClasses.disabled} {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

const Spot: NextPage = () => {
  const router = useRouter();

  return (
    <>
      <Header />

      <Box
        sx={{
          backgroundImage: `url("https://media-cdn.tripadvisor.com/media/photo-s/1a/98/29/4d/the-harp-bar.jpg")`,
          width: "100%",
          height: "400px",
          padding: "1rem",
          backgroundPosition: "center center",
          backgroundSize: "cover",
          boxShadow: "inset 0px 0px 250px 30000px rgb(0 0 0 / 50%)",
        }}
      >
        <Box
          sx={{
            width: "100%",
            height: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          <Typography
            variant="h1"
            sx={{
              textTransform: "unset",
              lineHeight: 1,
              mb: 2,
              color: "white",
              fontSize: "1.8rem",
            }}
          >
            La Canaille - île de Nantes
          </Typography>
          <Typography
            variant="body1"
            sx={{
              lineHeight: 1,
              color: "white",
              opacity: 0.9,
              fontWeight: 100,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <LocalOfferOutlined
              sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
            />{" "}
            Restaurant italien, Restauration rapide
          </Typography>
          <Box sx={{ mt: 1, display: "flex" }}>
            <Typography
              variant="body1"
              sx={{
                lineHeight: 1,
                color: "white",
                opacity: 0.9,
                fontWeight: 100,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                mr: 2,
              }}
            >
              <PlaceOutlined sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }} />{" "}
              Nantes
            </Typography>
            <Typography
              variant="body1"
              sx={{
                lineHeight: 1,
                color: "white",
                opacity: 0.9,
                fontWeight: 100,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <LanguageOutlined
                sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
              />{" "}
              <Link
                sx={{
                  color: "white",
                  textDecoration: "unset",
                  "&:hover": {
                    opacity: 0.8,
                  },
                }}
                href="https://google.com"
                target="_blank"
              >
                Site internet
              </Link>
            </Typography>
          </Box>
        </Box>

        <Box
          sx={{
            width: "200px",
            height: "200px",
            background: "white",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "1rem",
            position: "relative",
            top: "-6.5rem",
            zIndex: 999,
            boxShadow: "0px 0px 50px rgb(0 0 0 / 10%)",
          }}
        >
          <Box
            component="img"
            src="https://lamacompta.co/wp-content/uploads/2021/10/Logo-Geirec.png"
            sx={{
              maxWidth: "90%",
              maxHeight: "90%",
            }}
          />
        </Box>
      </Box>
      <TabsUnstyled defaultValue={0}>
        <Box
          className="container"
          sx={{
            width: "100%",
            height: "50px",
            mb: 6,
            background: "#F2F2F2",
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <TabsListUnstyled
            style={{
              margin: "-0.25rem",
              marginLeft: "14rem",
              display: "flex",
              alignItems: "center",
            }}
          >
            <Tab>Informations</Tab>
            <Tab>Jobs</Tab>
          </TabsListUnstyled>
          <Button
            size="small"
            variant="outlined"
            endIcon={<KeyboardArrowRightOutlined />}
          >
            Candidature spontanée
          </Button>
        </Box>

        <Box sx={{ mt: 3, width: "100%", background: "white", pt: 2, pb: 2 }}>
          <Box className="container">
            <TabPanelUnstyled value={0}>
              <InformationsTab />
            </TabPanelUnstyled>
            <TabPanelUnstyled value={1}>Jobs</TabPanelUnstyled>
          </Box>
        </Box>
      </TabsUnstyled>

      {/* <span>{router.query.spotSlug}</span> */}
    </>
  );
};

export default Spot;
