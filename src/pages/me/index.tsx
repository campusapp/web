import type { NextPage } from "next";
import { Header, UserProfile } from "../../components";
import { useUserContext } from "../../contexts";

const Me: NextPage = () => {
  const { userID } = useUserContext();

  console.log(userID);

  return (
    <>
      {userID && (
        <>
          <Header />
          <UserProfile />
        </>
      )}
    </>
  );
};

export default Me;
