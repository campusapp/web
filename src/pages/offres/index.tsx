import { Typography } from "@mui/material";
import type { NextPage } from "next";
import { Header } from "../../components";

const Spot: NextPage = () => {
  return (
    <>
      <Header />

      <Typography variant="h1">Page des spots</Typography>
    </>
  );
};

export default Spot;
