import {
  FavoriteBorder,
  Favorite,
  Launch,
  LocalOfferOutlined,
  KeyboardArrowRightOutlined,
  PlaceOutlined,
  LanguageOutlined,
  AssignmentOutlined,
  EventAvailableOutlined,
  Circle,
  AccessTimeOutlined,
  StarBorderOutlined,
  MenuBookOutlined,
  EuroOutlined,
  DataObjectRounded,
} from "@mui/icons-material";
import {
  Typography,
  Box,
  Chip,
  Stack,
  IconButton,
  Divider,
  Button,
  TabPanelUnstyled,
  TabUnstyled,
  TabsUnstyled,
  TabsListUnstyled,
  tabUnstyledClasses,
  buttonUnstyledClasses,
  Link,
} from "@mui/material";
import type { NextPage } from "next";
import { useRouter } from "next/dist/client/router";
import { styled } from "@mui/system";
import { useEffect, useMemo, useState } from "react";
import { Badge, Header } from "../../../components";
import axios from "axios";
import { useQuery } from "react-query";
import { getJobOffer } from "../../../services";

const Offer: NextPage = () => {
  const router = useRouter();

  const { isLoading, isError, data, error } = useQuery(["offer"], () =>
    getJobOffer()
  );

  const offer = data || [];

  return (
    <>
      <Header />

      <Box
        sx={{
          background: `#21806d`,
          width: "100%",
          padding: "3rem",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          mb: 6,
        }}
      >
        <Box
          sx={{
            width: "5rem",
            height: "5rem",
            background: "white",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            borderRadius: "999rem",
            boxShadow: "0px 0px 50px rgb(0 0 0 / 10%)",
          }}
        >
          <Box
            component="img"
            src="https://lamacompta.co/wp-content/uploads/2021/10/Logo-Geirec.png"
            sx={{
              maxWidth: "80%",
              maxHeight: "80%",
            }}
          />
        </Box>
        <Typography
          variant="h6"
          sx={{
            textTransform: "unset",
            fontStyle: "unset",
            lineHeight: 1,
            color: "white",
            fontSize: "1.8rem",
            mt: 2,
          }}
        >
          La Canaille - île de Nantes
        </Typography>
        <Typography
          variant="h2"
          align="center"
          sx={{
            textTransform: "unset",
            lineHeight: 1,
            mt: 4,
            color: "white",
            fontSize: "1rem",
          }}
        >
          {offer.title}
        </Typography>
        <Box sx={{ mt: 3, display: "flex" }}>
          <Typography
            variant="body1"
            sx={{
              lineHeight: 1,
              color: "white",
              opacity: 0.9,
              fontWeight: 100,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              mr: 2,
            }}
          >
            <AssignmentOutlined
              sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
            />{" "}
            {offer.contract_type}
          </Typography>
          <Typography
            variant="body1"
            sx={{
              lineHeight: 1,
              color: "white",
              opacity: 0.9,
              fontWeight: 100,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              mr: 2,
            }}
          >
            <PlaceOutlined sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }} />{" "}
            {offer.location}
          </Typography>
          <Typography
            variant="body1"
            sx={{
              lineHeight: 1,
              color: "white",
              opacity: 0.9,
              fontWeight: 100,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              mr: 2,
            }}
          >
            <EventAvailableOutlined
              sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
            />{" "}
            {offer.contract_dates}
          </Typography>
        </Box>
        <Button
          variant="outlined"
          color="primary"
          size="small"
          sx={{
            mt: 3,
            color: "white",
            borderColor: "white",
            "&:hover": {
              borderColor: "white",
            },
          }}
        >
          Postuler
        </Button>
      </Box>
      <Box className="container" sx={{ display: "flex" }}>
        <Box sx={{ width: "20rem", mr: 4 }}>
          <Typography
            variant="boy2"
            sx={{
              textTransform: "uppercase",
              fontWeight: "bold",
              opacity: 0.5,
              mb: 2,
            }}
          >
            Le spot
          </Typography>
          <Box
            sx={{
              display: "flex",
              mt: 2,
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Box
              sx={{
                minWidth: "5rem",
                minHeight: "5rem",
                maxWidth: "5rem",
                maxHeight: "5rem",
                background: "white",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                borderRadius: "8rem",
                boxShadow: "0px 0px 50px rgb(0 0 0 / 10%)",
                mr: 2,
              }}
            >
              <Box
                component="img"
                src="https://lamacompta.co/wp-content/uploads/2021/10/Logo-Geirec.png"
                sx={{
                  maxWidth: "80%",
                  maxHeight: "80%",
                }}
              />
            </Box>
            <Typography
              variant="h6"
              sx={{
                textTransform: "unset",
                fontStyle: "unset",
                lineHeight: 1,
                fontSize: "1.8rem",
              }}
            >
              La Canaille - île de Nantes
            </Typography>
          </Box>
          <Divider sx={{ width: "100%", mt: 2, mb: 2 }} />
          <Typography
            variant="boy2"
            sx={{
              textTransform: "uppercase",
              fontWeight: "bold",
              opacity: 0.5,
              mb: 2,
            }}
          >
            Récap'
          </Typography>
          <Box
            sx={{
              background: `#21806d`,
              width: "100%",
              padding: "2rem",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
              mt: 2,
              borderRadius: 2,
              color: "white",
            }}
          >
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
              }}
            >
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Box
                  component="span"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    mb: 1,
                  }}
                >
                  <AssignmentOutlined
                    sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
                  />
                  <i>Type de contrat : </i>
                </Box>
                <Typography variant="boy1" sx={{ lineHeight: "normal" }}>
                  {offer.contract_type}
                </Typography>
              </Box>
              <Divider sx={{ mt: 1, mb: 1, width: "100%" }} />
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Box
                  component="span"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    mb: 1,
                  }}
                >
                  <AccessTimeOutlined
                    sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
                  />
                  <i>Nombre d'heures : </i>
                </Box>
                <Typography variant="boy1" sx={{ lineHeight: "normal" }}>
                  {offer.working_hours}h
                </Typography>
              </Box>
              <Divider sx={{ mt: 1, mb: 1, width: "100%" }} />
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Box
                  component="span"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    mb: 1,
                  }}
                >
                  <PlaceOutlined
                    sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
                  />
                  <i>Localisation : </i>
                </Box>
                <Typography variant="boy1" sx={{ lineHeight: "normal" }}>
                  {offer.location}
                </Typography>
              </Box>
              <Divider sx={{ mt: 1, mb: 1, width: "100%" }} />
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Box
                  component="span"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    mb: 1,
                  }}
                >
                  <StarBorderOutlined
                    sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
                  />
                  <i>Expérience requise : </i>
                </Box>
                <Typography variant="boy1" sx={{ lineHeight: "normal" }}>
                  {offer.required_experience}
                </Typography>
              </Box>
              <Divider sx={{ mt: 1, mb: 1, width: "100%" }} />
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Box
                  component="span"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    mb: 1,
                  }}
                >
                  <MenuBookOutlined
                    sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
                  />
                  <i>Niveau d'études : </i>
                </Box>
                <Typography variant="boy1" sx={{ lineHeight: "normal" }}>
                  {offer.study_level}
                </Typography>
              </Box>
              <Divider sx={{ mt: 1, mb: 1, width: "100%" }} />
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Box
                  component="span"
                  sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    mb: 1,
                  }}
                >
                  <EuroOutlined
                    sx={{ fontSize: "1rem", opacity: 0.9, mr: 0.5 }}
                  />
                  <i>Rémunération : </i>
                </Box>
                <Typography variant="boy1" sx={{ lineHeight: "normal" }}>
                  {offer.salary}
                </Typography>
              </Box>
            </Box>
            <Button
              variant="outlined"
              color="primary"
              size="small"
              sx={{
                mt: 3,
                color: "white",
                borderColor: "white",
                "&:hover": {
                  borderColor: "white",
                },
              }}
            >
              Postuler
            </Button>
          </Box>
        </Box>
        <Box sx={{ width: "100%", flex: 1 }}>
          <Box>
            <Typography
              variant="h4"
              sx={{
                textTransform: "unset",
              }}
            >
              <Circle sx={{ fontSize: "1rem", color: "primary.main" }} /> À
              propos de l'entreprise
            </Typography>
            <Divider sx={{ width: "100%", mt: 1, mb: 2 }} />
            <Typography
              variant="body1"
              sx={{
                textTransform: "unset",
              }}
            >
              À propos de l'entreprise
            </Typography>
            <Button
              sx={{ mt: 2 }}
              variant="contained"
              endIcon={<KeyboardArrowRightOutlined />}
            >
              Découvrir La Canaille - Île de Nantes
            </Button>
          </Box>
          <Box sx={{ mt: 6 }}>
            <Typography
              variant="h4"
              sx={{
                textTransform: "unset",
              }}
            >
              <Circle sx={{ fontSize: "1rem", color: "primary.main" }} />{" "}
              Descriptif du poste
            </Typography>
            <Divider sx={{ width: "100%", mt: 1, mb: 2 }} />
            <Typography
              variant="body1"
              sx={{
                textTransform: "unset",
              }}
            >
              {offer.job_description}
            </Typography>
          </Box>
          <Box sx={{ mt: 6 }}>
            <Typography
              variant="h4"
              sx={{
                textTransform: "unset",
              }}
            >
              <Circle sx={{ fontSize: "1rem", color: "primary.main" }} /> Profil
              recherché
            </Typography>
            <Divider sx={{ width: "100%", mt: 1, mb: 2 }} />
            <Typography
              variant="body1"
              sx={{
                textTransform: "unset",
              }}
            >
              {offer.required_profile}
            </Typography>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Offer;
