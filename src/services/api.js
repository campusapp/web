import axios from "axios";

export const logoutUser = async () => {
  return axios({
    method: "post",
    url: `http://localhost:4000/api/user/logout`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};
