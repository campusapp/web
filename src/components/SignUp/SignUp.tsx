import { FC } from "react";
import { Button, Stack, TextField, Typography } from "@mui/material";
import Head from "next/head";
import axios from "axios";
import { useSnackbar } from "notistack";
import { useRouter } from "next/dist/client/router";
import { useFormik } from "formik";
import * as yup from "yup";

const registerSchema = yup.object().shape({
  username: yup
    .string()
    .max(4, "Le nom d'utilisateur est trop court (min 4 caractères)")
    .max(20, "Le nom d'utilisateur est trop long (max 20 caractères)")
    .required("Le nom d'utilisateur est obligatoire"),
  email: yup.string().required("L'adresse email est obligatoire"),
});

export const SignUp: FC = () => {
  const { enqueueSnackbar } = useSnackbar();

  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      username: "",
      email: "",
      password: "",
      controlPassword: "",
      role: "superuser",
    },
    validationSchema: registerSchema,
    onSubmit: async (values) => {
      try {
        await axios({
          method: "post",
          url: `http://localhost:4000/api/user/register`,
          withCredentials: true,
          data: {
            username: values.username,
            email: values.email,
            password: values.password,
            role: "superuser",
          },
        });
        enqueueSnackbar("Tout se passe comme prévu maintenant, connecte toi", {
          variant: "success",
        });
      } catch (error) {
        enqueueSnackbar("Aie coup dur la mauvaise note", {
          variant: "success",
        });
        console.error("error: ", error);
      }
    },
  });

  return (
    <>
      <Head>
        <title>Inscription</title>
      </Head>

      <form onSubmit={formik.handleSubmit}>
        <Typography variant="h3" sx={{ mb: 4, textAlign: "center" }}>
          Inscription
        </Typography>
        <div className="error"></div>
        <Stack spacing={3}>
          <TextField
            label="Nom d'utilisateur"
            variant="outlined"
            fullWidth
            required
            name="username"
            onChange={formik.handleChange}
            value={formik.values.username}
            error={formik.touched.username && Boolean(formik.errors.username)}
            helperText={formik.touched.username && formik.errors.username}
          />
          <TextField
            label="Adresse e-mail"
            variant="outlined"
            required
            type="email"
            fullWidth
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            label="Mot de passe"
            variant="outlined"
            required
            type="password"
            fullWidth
            name="password"
            onChange={formik.handleChange}
            value={formik.values.password}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
          />
          <TextField
            label="Vérification du mot de passe"
            variant="outlined"
            required
            type="password"
            fullWidth
            name="controlPassword"
            onChange={formik.handleChange}
            value={formik.values.controlPassword}
            error={
              formik.touched.controlPassword &&
              Boolean(formik.errors.controlPassword)
            }
            helperText={
              formik.touched.controlPassword && formik.errors.controlPassword
            }
          />
        </Stack>
        <Button
          type="submit"
          variant="contained"
          sx={{ margin: "0 auto", display: "block", mt: 2 }}
        >
          S'inscrire
        </Button>
      </form>
    </>
  );
};
