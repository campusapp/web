import { FC } from "react";
import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";

export const Badge: FC = () => {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          background: "#F8F8F8",
          height: "5rem",
          width: "5rem",
          borderRadius: "0.5rem",
          mb: 1,
        }}
      >
        <Box component="span" sx={{ fontSize: "2.5rem" }}>
          💚
        </Box>
        <Box component="span" sx={{ fontSize: "0.6rem" }}>
          8,6/10
        </Box>
      </Box>
      <Typography variant="body2" sx={{ lineHeight: 1 }}>
        Sécurité
      </Typography>
    </Box>
  );
};
