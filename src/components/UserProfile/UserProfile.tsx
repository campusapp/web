import { FC, useState } from "react";
import { Typography, Tabs, Tab, Box, Paper } from "@mui/material";
import Head from "next/head";
import { useUserContext } from "../../../src/contexts";

import { useRouter } from "next/dist/client/router";
import { EtablishmentsList } from "./_internal/establishments/EtablishmentsList";
import { JobOffersListList } from "./_internal/jobOffers/JobOffersList";
import { UsersList } from "./_internal/users/UsersList";

import { SportsTab } from "./_internal/SportsTab/SportsTab";
import { DashboardTab } from "./_internal/DashboardTab/DashboardTab";

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      style={{ width: "100%" }}
      {...other}
    >
      {value === index && (
        <Paper elevation={0} sx={{ ml: 3, bg: "white" }}>
          {children}
        </Paper>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export const UserProfile: FC = () => {
  const router = useRouter();
  const [value, setValue] = useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const { userID, user } = useUserContext();

  return (
    <>
      <Head>
        <title>Profile</title>
      </Head>
      <Box className="container">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="Dashboard" {...a11yProps(0)} />
          <Tab label="Sports" {...a11yProps(2)} />
          <Tab label="Offres" {...a11yProps(3)} />
          <Tab label="Utilisateurs" {...a11yProps(4)} />
        </Tabs>
        <TabPanel value={value} index={0}>
          <span>Mon ID d'utilisateur: {user?._id}</span>
          <DashboardTab />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <SportsTab />
        </TabPanel>
        <TabPanel value={value} index={2}>
          <JobOffersListList />
        </TabPanel>
        <TabPanel value={value} index={3}>
          <UsersList />
        </TabPanel>
      </Box>
    </>
  );
};
