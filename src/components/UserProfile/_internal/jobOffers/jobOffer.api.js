import axios from "axios";

export const getAll = async () => {
  return axios({
    method: "get",
    url: `http://localhost:4000/api/job-offer`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};

export const deleteOne = async (offerId) => {
  return axios({
    method: "delete",
    url: `http://localhost:4000/api/job-offer/${offerId}`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};
