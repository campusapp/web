import { FC, useState } from "react";
import {
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
} from "@mui/material";
import Head from "next/head";
import { useQuery } from "react-query";
import { getAll, deleteOne } from "./jobOffer.api";

import { useRouter } from "next/dist/client/router";
import { Close } from "@mui/icons-material";
import { useSnackbar } from "notistack";

export const JobOffersListList: FC = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const { isLoading, data } = useQuery(["jobOffers"], () => getAll());

  const jobOffers = data?.data;

  return (
    <>
      <Head>
        <title>Offres d'emploi</title>
      </Head>
      {jobOffers?.length > 0 && (
        <span>Offres d'emploi : ({jobOffers?.length}) :</span>
      )}
      {jobOffers?.length && (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="left">Date de publication</TableCell>
                  <TableCell align="left">ID</TableCell>
                  <TableCell align="left">Titre</TableCell>
                  <TableCell align="left">Spot</TableCell>
                  <TableCell align="left">Dates du contrat</TableCell>
                  <TableCell align="left">Status</TableCell>
                  <TableCell align="center"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {jobOffers.map((offer) => (
                  <TableRow
                    key={offer._id}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell align="left" component="th" scope="row">
                      {new Date(offer.createdAt).toLocaleDateString()}
                    </TableCell>
                    <TableCell align="left">{offer._id}</TableCell>
                    <TableCell align="left">{offer.title}</TableCell>
                    <TableCell align="left">{offer.etablishment_id}</TableCell>
                    <TableCell align="left">{offer.contract_dates}</TableCell>
                    <TableCell align="left">{offer.status}</TableCell>
                    <TableCell align="left">
                      <IconButton color="error">
                        <Close
                          color="error"
                          onClick={async () => {
                            try {
                              await deleteOne(offer._id);
                              enqueueSnackbar("Tout se passe comme prévu", {
                                variant: "success",
                              });
                            } catch (error) {
                              enqueueSnackbar("Aie coup dur la mauvaise note", {
                                variant: "success",
                              });
                            }
                          }}
                        />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </>
  );
};
