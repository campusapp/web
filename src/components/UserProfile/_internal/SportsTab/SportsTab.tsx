import { FC, useState } from "react";
import { Typography, Box, Paper, Stack, Button } from "@mui/material";
import { useQuery } from "react-query";
import { deleteOne, getAll } from "./_internal/sport.api";

import { useRouter } from "next/dist/client/router";
import { AddSportModal } from "./_internal/AddSportModal";

export const SportsTab: FC = () => {
  const router = useRouter();

  const { isLoading, data } = useQuery(["sports"], () => getAll());

  const sports = data?.data;

  return (
    <>
      <Typography variant="h3" component="h3" sx={{ mb: 2 }}>
        Sports ({sports?.length})
      </Typography>
      <AddSportModal />

      {sports?.length && (
        <Stack direction="row" gap={2} sx={{ flexWrap: "wrap", mt: 2 }}>
          {sports.map((sport, i) => (
            <Paper key={i}>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                }}
              >
                <Typography component="h6" variant="h6">
                  {sport.name}
                </Typography>
                <Typography component="span" variant="body2">
                  {sport._id}
                </Typography>
                <Typography component="span" variant="body2">
                  {sport.slug}
                </Typography>
                {sport.image && (
                  <Box
                    sx={{
                      width: "100%",
                      maxWidth: "10rem",
                      borderRadius: 2,
                      mt: 1,
                    }}
                    component="img"
                    src={sport.image}
                  />
                )}
              </Box>
              <Button
                sx={{ m: "0 auto", display: "block", mt: 2 }}
                onClick={() => deleteOne(sport._id)}
              >
                X
              </Button>
            </Paper>
          ))}
        </Stack>
      )}
    </>
  );
};
