import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
// import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { Stack, Typography } from "@mui/material";
import { useFormik } from "formik";
import axios from "axios";
import { Box } from "@mui/system";
import { useSnackbar } from "notistack";
// import { useDropzone } from "react-dropzone";

const toSlug = (text) => {
  return text
    .toLowerCase()
    .replace(/[^\w ]+/g, "")
    .replace(/ +/g, "-");
};

// const baseStyle = {
//   flex: 1,
//   display: "flex",
//   flexDirection: "column",
//   alignItems: "center",
//   padding: "20px",
//   borderWidth: 2,
//   borderRadius: 2,
//   borderColor: "#eeeeee",
//   borderStyle: "dashed",
//   backgroundColor: "#fafafa",
//   color: "#bdbdbd",
//   outline: "none",
//   transition: "border .24s ease-in-out",
// };
// const focusedStyle = {
//   borderColor: "#2196f3",
// };
// const acceptStyle = {
//   borderColor: "#00e676",
// };
// const rejectStyle = {
//   borderColor: "#ff1744",
// };

export const AddSportModal: React.FC = () => {
  const [open, setOpen] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();

  // const [uploadedImage, setUploadedImage] = React.useState(null);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      slug: "",
      image: "",
    },
    onSubmit: async (values) => {
      try {
        await axios({
          method: "post",
          url: `http://localhost:4000/api/sport`,
          withCredentials: true,
          data: {
            name: values.name,
            slug: toSlug(values.name),
            image: values.image,
          },
        });
        formik.resetForm();
        handleClose();
        enqueueSnackbar("Tout se passe comme prévu", {
          variant: "success",
        });
      } catch (error) {
        enqueueSnackbar("Aie coup dur la mauvaise note", {
          variant: "success",
        });
        console.error("error: ", error);
      }
    },
  });

  // code pour l'umpload d'image vers le serveur ftp mais problème avec la clé rsa..

  // const handleDrop = async (acceptedFiles) => {
  //   console.log(acceptedFiles);
  //   const data = new FormData();
  //   data.append("file", acceptedFiles[0]);
  //   data.append("file_name", acceptedFiles[0].name);

  //   const response = await axios.post(
  //     "http://localhost:4000/api/sport/upload",
  //     data
  //   );
  //   console.log(response);
  //   setUploadedImage(response.data);
  // };

  // const { getRootProps, getInputProps, isFocused, isDragAccept, isDragReject } =
  //   useDropzone({ accept: "image/*", onDrop: handleDrop, multiple: false });

  // const style = React.useMemo(
  //   () => ({
  //     ...baseStyle,
  //     ...(isFocused ? focusedStyle : {}),
  //     ...(isDragAccept ? acceptStyle : {}),
  //     ...(isDragReject ? rejectStyle : {}),
  //   }),
  //   [isFocused, isDragAccept, isDragReject]
  // );

  return (
    <Box component="form">
      <Button variant="outlined" onClick={handleClickOpen}>
        Ajouter un sport
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Typography variant="h4" componant="h4" textAlign="center">
            Ajouter un sport
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Stack gap={3}>
            <TextField
              name="name"
              onChange={formik.handleChange}
              value={formik.values.name}
              label="Nom"
              variant="outlined"
              fullWidth
            />
            <TextField
              name="slug"
              onChange={formik.handleChange}
              value={toSlug(formik.values.name)}
              label="Slug"
              variant="outlined"
              fullWidth
              disabled
            />
            <TextField
              name="image"
              onChange={formik.handleChange}
              value={formik.values.image}
              label="Image"
              variant="outlined"
              fullWidth
            />

            {/* Porblème avec la clé RSA upload d'image.. domage */}

            {/* <div {...getRootProps({ style })}>
              <input {...getInputProps()} />
              <p>Glissez-déposez vos fichiers ici</p>
            </div> */}

            {/* {uploadedImage && (
              <img
                src={`http://localhost:4000/${uploadedImage.path}`}
                width={400}
              />
            )} */}
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => formik.handleSubmit()} type="submit">
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
