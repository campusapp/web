import axios from "axios";

export const getAll = async () => {
  return axios({
    method: "get",
    //   url: `${process.env.REACT_APP_API_URL}/api/etablishment`,
    url: `http://localhost:4000/api/sport`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};

export const deleteOne = async (sportId) => {
  return axios({
    method: "delete",
    //   url: `${process.env.REACT_APP_API_URL}/api/etablishment`,
    url: `http://localhost:4000/api/sport/${sportId}`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};
