import { FC } from "react";
import { Typography, Box, Paper, Stack, IconButton } from "@mui/material";
import { useQuery } from "react-query";

import { useRouter } from "next/dist/client/router";
import { AddClubModal } from "./_internal/AddClubModal";
import { deleteOne, getAll, getByUserId } from "./_internal/club.api";
import { useUserContext } from "../../../../contexts";
import { ClearOutlined, EditOutlined } from "@mui/icons-material";
import { EditClubModal } from "./_internal/EditClubModal";

const AllClubs = () => {
  const { isLoading, data } = useQuery(["allClubs"], () => getAll());

  const allClubs = data?.data;

  return (
    <>
      <Typography variant="h3" component="h3" sx={{ mb: 2, mt: 4 }}>
        Vous êtes superuser, tous les clubs ({allClubs?.length || "0"}) :
      </Typography>
      {allClubs?.length ? (
        <Stack gap={2}>
          {allClubs.map((club, i) => (
            <Paper key={i}>
              <Box sx={{ display: "flex", alignIteams: "center" }}>
                <Box
                  sx={{
                    width: "3rem",
                    height: "3rem",
                    objectFit: "contain",
                    mr: 2,
                  }}
                  component="img"
                  src={club.logo}
                />
                <Box>
                  <Typography variant="body1" component="p">
                    {club.name}
                  </Typography>
                  <Typography
                    variant="body2"
                    component="p"
                    sx={{ fontSize: "0.8rem" }}
                  >
                    {club._id}
                  </Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  mt: 2,
                  display: "flex",
                  alignIteams: "center",
                  justifyContent: "center",
                }}
              >
                <EditClubModal club={club} />
                <IconButton color="error" onClick={() => deleteOne(club._id)}>
                  <ClearOutlined />
                </IconButton>
              </Box>
            </Paper>
          ))}
        </Stack>
      ) : (
        <Typography variant="body1" component="p">
          Il n'y a aucun club
        </Typography>
      )}
    </>
  );
};

const UserClubs = ({ ...rest }) => {
  const { userID } = useUserContext();

  const { isLoading, data } = useQuery(["userClubs"], () =>
    getByUserId(userID)
  );

  const userClubs = data?.data;

  return (
    <Box {...rest}>
      {userClubs?.length ? (
        <Stack gap={2}>
          {userClubs.map((club, i) => (
            <Paper key={i}>
              <Box sx={{ display: "flex", alignIteams: "center" }}>
                <Box
                  sx={{
                    width: "3rem",
                    height: "3rem",
                    objectFit: "contain",
                    mr: 2,
                  }}
                  component="img"
                  src={club.logo}
                />
                <Box>
                  <Typography variant="body1" component="p">
                    {club.name}
                  </Typography>
                  <Typography
                    variant="body2"
                    component="p"
                    sx={{ fontSize: "0.8rem" }}
                  >
                    {club._id}
                  </Typography>
                </Box>
              </Box>
              <Box
                sx={{
                  mt: 2,
                  display: "flex",
                  alignIteams: "center",
                  justifyContent: "center",
                }}
              >
                <EditClubModal club={club} />
                <IconButton color="error" onClick={() => deleteOne(club._id)}>
                  <ClearOutlined />
                </IconButton>
              </Box>
            </Paper>
          ))}
        </Stack>
      ) : (
        <Typography variant="body1" component="p">
          Il n'y a aucun club
        </Typography>
      )}
    </Box>
  );
};

export const DashboardTab: FC = () => {
  const router = useRouter();
  const { user, userID } = useUserContext();

  return (
    <>
      <Typography variant="h3" component="h3" sx={{ mb: 2, mt: 2 }}>
        Vos clubs
      </Typography>

      <AddClubModal />

      <UserClubs sx={{ marginTop: 2 }} />

      {user?.role === "superuser" && <AllClubs />}
    </>
  );
};
