import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Stack, InputLabel, Typography, IconButton } from "@mui/material";
import { useFormik } from "formik";
import axios from "axios";
import { Box } from "@mui/system";
import { useDropzone } from "react-dropzone";
import { EditOutlined } from "@mui/icons-material";
import { useSnackbar } from "notistack";

const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};
const focusedStyle = {
  borderColor: "#2196f3",
};
const acceptStyle = {
  borderColor: "#00e676",
};
const rejectStyle = {
  borderColor: "#ff1744",
};

export const EditClubModal: React.FC = ({ club }) => {
  const [open, setOpen] = React.useState(false);
  const { enqueueSnackbar } = useSnackbar();

  const [uploadedImage, setUploadedImage] = React.useState(null);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const formik = useFormik({
    initialValues: {
      name: club.name || "",
      organization_type: "club",
      sport_id: club.sport_id || "",
      address: club.address || "",
      logo: club.logo || "",
      short_name: club.short_name || "",
      presentation: club.presentation || "",
      color_primary: club.color_primary || "",
      color_secondary: club.color_secondary || "",
      phone: club.phone || "",
      email: club.email || "",
      instagram: club.instagram || "",
      facebook: club.facebook || "",
      twitter: club.twitter || "",
      tiktok: club.tiktok || "",
    },
    onSubmit: async (values) => {
      try {
        await axios({
          method: "put",
          //   url: `${process.env.REACT_APP_API_URL}/api/user/login`,
          url: `http://localhost:4000/api/organization/${club._id}`,
          withCredentials: true,
          data: {
            name: values.name,
            organization_type: "club",
            sport_id: values.sport_id,
            address: values.address,
            logo: values.logo,
            short_name: values.short_name,
            presentation: values.presentation,
            color_primary: values.color_primary,
            color_secondary: values.color_secondary,
            phone: values.phone,
            email: values.email,
            instagram: values.instagram,
            facebook: values.facebook,
            twitter: values.twitter,
            tiktok: values.tiktok,
          },
        });
        handleClose();
        enqueueSnackbar("Tout se passe comme prévu", {
          variant: "success",
        });
      } catch (error) {
        enqueueSnackbar("Aie coup dur la mauvaise note", {
          variant: "success",
        });
        console.error("error: ", error);
      }
    },
  });

  const handleDrop = async (acceptedFiles) => {
    const data = new FormData();
    data.append("file", acceptedFiles[0]);
    data.append("file_name", acceptedFiles[0].name);

    const response = await axios.post(
      "http://localhost:4000/api/sport/upload",
      data
    );
    setUploadedImage(response.data);
  };

  const { getRootProps, getInputProps, isFocused, isDragAccept, isDragReject } =
    useDropzone({ accept: "image/*", onDrop: handleDrop, multiple: false });

  const style = React.useMemo(
    () => ({
      ...baseStyle,
      ...(isFocused ? focusedStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isFocused, isDragAccept, isDragReject]
  );

  return (
    <Box component="form">
      <IconButton onClick={handleClickOpen}>
        <EditOutlined />
      </IconButton>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Typography
            component="h4"
            variant="h4"
            sx={{ textAlign: "center", textTransform: "uppercase" }}
          >
            Modifier {club.name}
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ p: 0, minWidth: "22rem" }}>
          <Stack gap={2}>
            <Box>
              <InputLabel>Nom du club</InputLabel>
              <TextField
                name="name"
                onChange={formik.handleChange}
                value={formik.values.name}
                variant="outlined"
                fullWidth
              />
            </Box>
            <Box>
              <InputLabel>Nom court</InputLabel>
              <TextField
                fullWidth
                name="short_name"
                variant="outlined"
                defaultValue={formik.values.short_name}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Adresse</InputLabel>
              <TextField
                fullWidth
                name="address"
                variant="outlined"
                defaultValue={formik.values.address}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Sport</InputLabel>
              <TextField
                fullWidth
                name="sport_id"
                variant="outlined"
                defaultValue={formik.values.sport_id}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>
                Logo du club (ne pas utiliser ne fonctionne pas)
              </InputLabel>
              <div {...getRootProps({ style })}>
                <input {...getInputProps()} />
                <p>Glissez-déposez le logo ici</p>
              </div>

              {uploadedImage && (
                <Box
                  component="img"
                  src={`http://localhost:4000/${uploadedImage.path}`}
                  width={400}
                />
              )}
            </Box>
            <Box>
              <InputLabel>Présentation</InputLabel>
              <TextField
                fullWidth
                multiline
                rows={4}
                name="presentation"
                variant="outlined"
                defaultValue={formik.values.presentation}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Numéro de téléphone</InputLabel>
              <TextField
                fullWidth
                type="tel"
                name="phone"
                variant="outlined"
                defaultValue={formik.values.phone}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Adresse email</InputLabel>
              <TextField
                fullWidth
                type="email"
                name="email"
                variant="outlined"
                defaultValue={formik.values.email}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Instagram</InputLabel>
              <TextField
                fullWidth
                name="instagram"
                variant="outlined"
                defaultValue={formik.values.instagram}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Facebook</InputLabel>
              <TextField
                fullWidth
                name="facebook"
                variant="outlined"
                defaultValue={formik.values.facebook}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Twitter</InputLabel>
              <TextField
                fullWidth
                name="twitter"
                variant="outlined"
                defaultValue={formik.values.twitter}
                onChange={formik.handleChange}
              />
            </Box>
            <Box>
              <InputLabel>Tiktok</InputLabel>
              <TextField
                fullWidth
                name="tiktok"
                variant="outlined"
                defaultValue={formik.values.tiktok}
                onChange={formik.handleChange}
              />
            </Box>
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            type="submit"
            onClick={() => formik.handleSubmit()}
          >
            Modifier
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
