import * as React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { Stack, InputLabel, Typography, Select, MenuItem } from "@mui/material";
import { useFormik } from "formik";
import axios from "axios";
import { Box } from "@mui/system";
import { useDropzone } from "react-dropzone";
import { useUserContext } from "../../../../../contexts";
import { getSports } from "./club.api";
import { useQuery } from "react-query";
import { useSnackbar } from "notistack";

// const baseStyle = {
//   flex: 1,
//   display: "flex",
//   flexDirection: "column",
//   alignItems: "center",
//   padding: "20px",
//   borderWidth: 2,
//   borderRadius: 2,
//   borderColor: "#eeeeee",
//   borderStyle: "dashed",
//   backgroundColor: "#fafafa",
//   color: "#bdbdbd",
//   outline: "none",
//   transition: "border .24s ease-in-out",
// };
// const focusedStyle = {
//   borderColor: "#2196f3",
// };
// const acceptStyle = {
//   borderColor: "#00e676",
// };
// const rejectStyle = {
//   borderColor: "#ff1744",
// };

export const AddClubModal: React.FC = () => {
  const [open, setOpen] = React.useState(false);
  const { userID } = useUserContext();
  const { enqueueSnackbar } = useSnackbar();

  // const [uploadedImage, setUploadedImage] = React.useState(null);

  const { isLoading, data } = useQuery(["sports"], () => getSports());

  const sports = data?.data;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    formik.resetForm();
  };

  const formik = useFormik({
    initialValues: {
      name: "",
      organization_type: "club",
      sport_id: "",
      address: "",
      logo: "https://pbs.twimg.com/media/DhnHmSFXkAAfv9A.jpg",
      created_by_user_id: userID,
    },
    onSubmit: async (values) => {
      try {
        await axios({
          method: "post",
          url: `http://localhost:4000/api/organization`,
          withCredentials: true,
          data: {
            name: values.name,
            organization_type: "club",
            sport_id: values.sport_id,
            address: values.address,
            logo: values.logo,
            created_by_user_id: userID,
          },
        });
        formik.resetForm();
        handleClose();
        enqueueSnackbar("Tout se passe comme prévu", {
          variant: "success",
        });
      } catch (error) {
        enqueueSnackbar("Aie coup dur la mauvaise note", {
          variant: "success",
        });
        console.error("error: ", error);
      }
    },
  });

  // const handleDrop = async (acceptedFiles) => {
  //   console.log(acceptedFiles);
  //   const data = new FormData();
  //   data.append("file", acceptedFiles[0]);
  //   data.append("file_name", acceptedFiles[0].name);

  //   const response = await axios.post(
  //     "http://localhost:4000/api/sport/upload",
  //     data
  //   );
  //   console.log(response);
  //   setUploadedImage(response.data);
  // };

  // const { getRootProps, getInputProps, isFocused, isDragAccept, isDragReject } =
  //   useDropzone({ accept: "image/*", onDrop: handleDrop, multiple: false });

  // const style = React.useMemo(
  //   () => ({
  //     ...baseStyle,
  //     ...(isFocused ? focusedStyle : {}),
  //     ...(isDragAccept ? acceptStyle : {}),
  //     ...(isDragReject ? rejectStyle : {}),
  //   }),
  //   [isFocused, isDragAccept, isDragReject]
  // );

  return (
    <Box component="form">
      <Button variant="outlined" onClick={handleClickOpen}>
        Ajouter un club
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Typography
            component="h4"
            variant="h4"
            sx={{ textAlign: "center", textTransform: "uppercase" }}
          >
            Ajouter un club
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ p: 0, minWidth: "22rem" }}>
          <Stack gap={2}>
            <Box>
              <InputLabel>Nom du club</InputLabel>
              <TextField
                name="name"
                onChange={formik.handleChange}
                value={formik.values.name}
                variant="outlined"
                fullWidth
              />
            </Box>
            <Box>
              <InputLabel>Adresse</InputLabel>
              <TextField
                fullWidth
                name="address"
                variant="outlined"
                defaultValue={formik.values.address}
                onChange={formik.handleChange}
              />
            </Box>

            <Box>
              <InputLabel>Sport</InputLabel>
              <Select
                fullWidth
                name="sport_id"
                variant="outlined"
                value={formik.values.sport_id}
                onChange={formik.handleChange}
              >
                {sports?.map((sport, i) => (
                  <MenuItem key={i} value={sport._id}>
                    {sport.name}
                  </MenuItem>
                ))}
              </Select>
            </Box>
            <Box>
              <InputLabel>Logo du club</InputLabel>
              <TextField
                fullWidth
                name="sport_id"
                variant="outlined"
                defaultValue={formik.values.logo}
                onChange={formik.handleChange}
              />
            </Box>

            {/* Ne fonctionne pas probleme cle rsa */}
            {/* <Box>
              <InputLabel>Logo du club</InputLabel>
              <div {...getRootProps({ style })}>
                <input {...getInputProps()} />
                <p>Glissez-déposez le logo ici</p>
              </div>

              {uploadedImage && (
                <img
                  src={`http://localhost:4000/${uploadedImage.path}`}
                  width={400}
                />
              )}
            </Box> */}
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            onClick={() => formik.handleSubmit()}
            type="submit"
          >
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
};
