import axios from "axios";

export const getAll = async () => {
  return axios({
    method: "get",
    //   url: `${process.env.REACT_APP_API_URL}/api/etablishment`,
    url: `http://localhost:4000/api/organization`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};

export const getByUserId = async (userId) => {
  return axios({
    method: "get",
    //   url: `${process.env.REACT_APP_API_URL}/api/etablishment`,
    url: `http://localhost:4000/api/organization/user`,
    withCredentials: true,
    data: {
      userId,
    },
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};

export const deleteOne = async (clubId) => {
  return axios({
    method: "delete",
    //   url: `${process.env.REACT_APP_API_URL}/api/etablishment`,
    url: `http://localhost:4000/api/organization/${clubId}`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};

export const getSports = async () => {
  return axios({
    method: "get",
    url: `http://localhost:4000/api/sport`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};
