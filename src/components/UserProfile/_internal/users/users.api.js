import axios from "axios";

export const getAll = async () => {
  return axios({
    method: "get",
    url: `http://localhost:4000/api/user`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};

export const deleteOne = async (userId) => {
  return axios({
    method: "delete",
    url: `http://localhost:4000/api/user/${userId}`,
    withCredentials: true,
  })
    .then((res) => {
      return { data: res.data };
    })
    .catch((error) => console.error(error));
};
