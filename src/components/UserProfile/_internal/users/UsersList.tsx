import { FC, useState } from "react";
import {
  Typography,
  Tabs,
  Tab,
  Box,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
} from "@mui/material";
import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import Head from "next/head";
import { useQuery } from "react-query";
import { deleteOne, getAll } from "./users.api";

import { useRouter } from "next/dist/client/router";
import { Close } from "@mui/icons-material";
import { useSnackbar } from "notistack";

export const UsersList: FC = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const { isLoading, data } = useQuery(["users"], () => getAll());

  const users = data?.data;

  return (
    <>
      <Head>
        <title>Utilisateurs</title>
      </Head>
      {users?.length > 0 && (
        <span>Liste des utilisateurs : ({users?.length}) :</span>
      )}
      {users?.length && (
        <>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="left">Date d'inscription</TableCell>
                  <TableCell align="left">ID</TableCell>
                  <TableCell align="left">Username</TableCell>
                  <TableCell align="left">email</TableCell>
                  <TableCell align="left">role</TableCell>
                  <TableCell align="center"></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {users.map((user) => (
                  <TableRow
                    key={user._id}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell align="left" component="th" scope="row">
                      {new Date(user.createdAt).toLocaleDateString()}
                    </TableCell>
                    <TableCell align="left">{user._id}</TableCell>
                    <TableCell align="left">{user.username}</TableCell>
                    <TableCell align="left">{user.email}</TableCell>
                    <TableCell align="left">
                      {user.role}{" "}
                      {user.role === "superuser" && "/ coucou petite peruche"}
                    </TableCell>
                    <TableCell align="left">
                      <IconButton
                        color="error"
                        disabled={user.role === "superuser"}
                      >
                        <Close
                          onClick={async () => {
                            try {
                              deleteOne(user._id);
                              enqueueSnackbar("Tout se passe comme prévu", {
                                variant: "success",
                              });
                            } catch (error) {
                              enqueueSnackbar("Aie coup dur la mauvaise note", {
                                variant: "success",
                              });
                            }
                          }}
                        />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </>
  );
};
