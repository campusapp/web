import { FC } from "react";
import { Menu } from "@mui/icons-material";
import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@mui/material";
import { useRouter } from "next/dist/client/router";
import { useUserContext } from "../../contexts";
import axios from "axios";
import { logoutUser } from "../../services";
import { useSnackbar } from "notistack";

export const Header: FC = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();

  const { userID } = useUserContext();

  const handleLogout = (e) => {
    try {
      logoutUser();
      enqueueSnackbar("Tout se passe comme prévu. Tu es déconecté", {
        variant: "success",
      });
      router.push("/");
    } catch (error) {
      enqueueSnackbar("Aie coup dur la mauvaise note", {
        variant: "success",
      });
    }
  };

  return (
    <AppBar position="static" color="default" sx={{ p: 0 }}>
      <Toolbar>
        <Box
          sx={{
            cursor: "pointer",
          }}
          onClick={() => router.push("/")}
        >
          <Box
            component="img"
            src="http://one-sport.paulclique.fr/wp-content/uploads/2021/09/cropped-logo-one.png"
            sx={{ height: "2rem", width: "auto" }}
          />
        </Box>

        {userID && (
          <Button color="inherit" onClick={(e) => handleLogout(e)}>
            Se déconnecter
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
};
