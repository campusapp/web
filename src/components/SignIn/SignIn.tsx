import { FC, useState } from "react";
import { Button, Stack, TextField, Typography } from "@mui/material";
import Head from "next/head";
import { Header } from "../../components";
import Image from "next/image";
import styles from "../../../styles/Home.module.css";
import axios from "axios";
import { useRouter } from "next/dist/client/router";

export const SignIn: FC = () => {
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = (e) => {
    const error = document.querySelector(".error");

    e.preventDefault();
    axios({
      method: "post",
      //   url: `${process.env.REACT_APP_API_URL}/api/user/login`,
      url: `http://localhost:4000/api/user/login`,
      withCredentials: true,
      data: {
        email,
        password,
      },
    })
      .then((res) => {
        if (res.data.errors) {
          error.innerHTML = res.data.errors.error;
        } else {
          router.push("/me");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      <form onSubmit={handleLogin}>
        <Typography variant="h3" sx={{ mb: 4, textAlign: "center" }}>
          Connexion
        </Typography>
        <div className="error"></div>
        <Stack spacing={3}>
          <TextField
            label="Adresse e-mail"
            variant="outlined"
            type="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <TextField
            label="Mot de passe"
            variant="outlined"
            type="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </Stack>
        <Button
          type="submit"
          variant="contained"
          sx={{ margin: "0 auto", display: "block", mt: 2 }}
        >
          Se connecter
        </Button>
      </form>
    </>
  );
};
